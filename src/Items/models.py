from django.db import models
import os
from Pricr.settings import BASE_DIR, STATIC_ROOT
import geocoder

# Create your models here.
class Item(models.Model):
	def user_directory_path(instance, filename):
    	# file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
   		return 'user_{0}/{1}'.format(instance.user.id, filename)


	item_price				=	models.DecimalField(max_digits=10, decimal_places=2,)
	item_name				=	models.CharField(max_length=50, unique=True)
	store					= 	models.ForeignKey('Store', on_delete=models.CASCADE,)	# This should be changed so that it relates to a db of stores
	proof_of_price			=	models.ImageField(upload_to='static/imageUploads/proof_of_price', height_field=None, width_field=None, max_length=100,) #FIXME images are not properly stored
	is_sale_price			=	models.BooleanField(default=False,)
	datetime_of_submission	=	models.DateTimeField(auto_now_add=True)
	image_of_item			=	models.ImageField(upload_to='static/imageUploads/image_of_tem', height_field=None, width_field=None, max_length=100, null=True) #FIXME images are not properly stored
	#user_changed_by	=	user model	

	def __str__(self):
		return "item name: " +str(self.item_name)+" id: "+str(self.id)	

class UpdateInformation(models.Model):
	item				=	models.ForeignKey('Item', related_name='UpdateInformation')
	datetime_of_update	=	models.DateTimeField(auto_now_add=True)
	#changes made
	#user_changed_by	=	user model	

class ItemAttribute(models.Model):
	item 			= 	models.ForeignKey('Item', related_name='ItemAttributes')
	attributeName	=	models.CharField(max_length=50, unique=True)
	attributeValue	=	models.CharField(max_length=100)
	attributeUnit	=	models.CharField(max_length=20, unique=True)


class Store(models.Model):
	name 			= 	models.ForeignKey('StoreName', on_delete=models.CASCADE)
	store_location	=	models.CharField(max_length=50,) #This should be replaced with a custom field that uses a location library to accurately place each store
	datetime_of_submission	=	models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return str(self.name)+ ' at '+ str(self.store_location)



class StoreName(models.Model):
	name 			= 	models.CharField(max_length=50, unique=True)
	datetime_of_submission	=	models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return str(self.name)


class Geocode(object):
	""" A location using the Geocoder library """

	def __init__(self, geocode_str, given_geocode_str):
		self.geocode_str=geocode_str
		self.given_geocode_str=given_geocode_str



class GeocodeField(models.CharField):
	description = "A specific location on earth"

	def __init__(self, *args, **kwargs):
		super(HandField, self).__init__(*args, **kwargs)

		
