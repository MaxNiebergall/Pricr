# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-22 19:10
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Items', '0010_item_item_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='item_URL',
        ),
    ]
