from django.contrib import admin

# Register your models here.
from .models import Item, Store, StoreName
admin.site.register(Item)
admin.site.register(Store)
admin.site.register(StoreName)