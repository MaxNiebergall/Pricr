from .models import Item
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector

def search(terms):
	# Leave this code, it is a more advaced search that sqlite databases do not support,
	# but postgres ones do, so we'll use it when we switch to the postgres database
	vector = SearchVector('item_name', weight='A') +\
	SearchVector('store__name__name', weight='A') +\
	SearchVector('store__store_location', weight='C')

	query = SearchQuery(terms)

	results = Item.objects.annotate(rank=SearchRank(vector, query)).filter(rank__gte=0.3)#.order_by('rank')

	# queries = terms.split()

	# results = None

	# for query in queries:
	# 	if not results:
	# 		results = Item.objects.filter(item_name__icontains=query) |\
	# 		Item.objects.filter(store__name__name__icontains=query) |\
	# 		Item.objects.filter(item_price__icontains=query) |\
	# 		Item.objects.filter(store__store_location__icontains=query)
	# 	else:
	# 		results = results | Item.objects.filter(item_name__icontains=query) |\
	# 		Item.objects.filter(store__name__name__icontains=query) |\
	# 		Item.objects.filter(item_price__icontains=query) |\
	# 		Item.objects.filter(store__store_location__icontains=query)

	if results is None or not results.exists():
		return 'Nothing'

	return results

def html_readable(queryset):
	html_readable_version = list()

	for item in queryset:
		result = {
			'price': item.item_price,
			'name': item.item_name,
			'store_name': item.store.name.name,
			'store_location': item.store.store_location,
			'is_sale_price': item.is_sale_price,
			'proof_of_price_url': item.proof_of_price.url
		}

		html_readable_version.append(result)

	return html_readable_version