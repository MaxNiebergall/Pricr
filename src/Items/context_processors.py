from .controllers import search
from .forms import SearchForm

def search_processor(request):
	search_form = SearchForm(request.GET or None)

	context = {
		'search_form' : search_form,
		'search' : '',
	}

	return context