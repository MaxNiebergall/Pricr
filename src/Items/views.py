from django.shortcuts import render
from django.views.generic import View
# Create your views here
from .forms import NewItemForm, SearchForm, NewStoreForm, NewStoreNameForm
from .models import Item, Store
from .controllers import search 
from django.template import RequestContext, Template
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
import geocoder

def createStoreName(request):
	if request.POST:
		newStoreNameForm = NewStoreNameForm(request.POST)
		if newStoreNameForm.is_valid():
			newStoreNameForm.save()
			return HttpResponseRedirect("/")
	else:
		newStoreNameForm = NewStoreNameForm(None)

	context = {
		"form": newStoreNameForm,
	}
	if newStoreNameForm.is_valid():
		newStoreNameForm.save()
		return render(request, "items/createStoreName.html", context)

	return render(request, "items/createStoreName.html", context)

def createStore(request):
	if request.POST:
		newStoreForm = NewStoreForm(request.POST)
		if newStoreForm.is_valid():
			newStoreForm.save()
			return HttpResponseRedirect("/")
	else:
		newStoreNameForm = NewStoreNameForm()
		newStoreForm = NewStoreForm(None)

	context = {
		"newStoreForm": newStoreForm,
		"newStoreNameForm": newStoreNameForm,
	}
	if newStoreForm.is_valid():
		newStoreForm.save()
		return render(request, "items/createStore.html", context)

	return render(request, "items/createStore.html", context)

def itemListView(request):
	context= {
		"object_list" : list(),
		"title" : "Item List",
		"is_search" : False,
	}
	
	for queryset in Item.objects.all():
		print(queryset)
		item_vals = {
			"list":	True,
			"item_name" : queryset.item_name,
			"store" : queryset.store,
			"store_id": queryset.store.id,
			"item_price" : queryset.item_price,
			"proof_of_price" : queryset.proof_of_price,
			"is_sale_price" : queryset.is_sale_price,
			"id": queryset.id,
		}

		context["object_list"].append(item_vals)

	return render(request, "items/itemListView.html", context)


def createItem(request):
	if request.POST:
		newItemForm = NewItemForm(request.POST, request.FILES)
		newStoreForm = NewStoreForm()
		newStoreNameForm = NewStoreNameForm()
		if newItemForm.is_valid():
			newItemForm.save()
			return HttpResponseRedirect("/")
	else:
		newItemForm = NewItemForm(None)
		newStoreForm = NewStoreForm(None)
		newStoreNameForm = NewStoreNameForm(None)


	if newItemForm.is_valid():
		newItemForm.save()
				
		return render(request, "items/createItem.html", context)

	context = {
		"newStoreForm": newStoreForm,
		"newStoreNameForm": newStoreNameForm,
		"newItemForm": newItemForm
	}
	
	return render(request, "items/createItem.html", context)

def index(request):
	newItemForm = NewItemForm()
	newStoreForm = NewStoreForm()
	newStoreNameForm = NewStoreNameForm()

	searchForm = SearchForm(request.GET or None)

	context = {
		"newItemForm": newItemForm,
		"newStoreForm": newStoreForm,
		"newStoreNameForm": newStoreNameForm
	}

	if searchForm.is_valid():
		context['object_list'] = search(searchForm.cleaned_data['search'])
		if len(object_list)==0:
			context['failed_to_find']="Your search did not return any results"
			context['is_search']= True

		return render(request,"items/itemListView.html", context)

	return render(request,"items/index.html", context)

def displayItem(request):
	try:
		query=request.path
		print(query)
		query=query.replace("/items/", "")
		print(query)
		queryset = Item.objects.get(id=query) 
		print(queryset)
		context= {
			"is_item": True,
			"item_name" : queryset.item_name,
			"store" : queryset.store,
			"item_price" : queryset.item_price,
			"proof_of_price" : queryset.proof_of_price,
			"is_sale_price" : queryset.is_sale_price,	
		}
	except:
		context={
			"failed_to_find": "We couldn't find the item you were looking for. If you believe this item should exist, please email a developer"
		}

	return render(request, "items/displayItem.html", context)


def displayStore(request):
	
	try:
		query=request.path
		query=query.replace("/stores/", "")
		queryset = Store.objects.get(id=query) 
		print("querryset",queryset)
		context= {
			"is_item": False,
			"store_location" : queryset.store_location,
			"store" : queryset.name,
		}
	except:
		context={
			"failed_to_find": "We couldn't find the store you were looking for. If you believe this store should exist, please email a developer"
		}

	return render(request, "items/displayItem.html", context)