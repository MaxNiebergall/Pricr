from django import forms

from .models import Item, Store, StoreName

from django.template.loader import render_to_string


class NewItemForm(forms.ModelForm):
	store = forms.ModelChoiceField(Store.objects)
	
	class Meta:
		model = Item
		fields= [
			"proof_of_price", "item_price", "item_name", "store", "is_sale_price", "image_of_item",
		]
	def __init__(self, *args, **kwargs):
		super(NewItemForm, self).__init__(*args, **kwargs)

		self.fields['item_price'].widget.attrs.update({'class' : 'form-control'})
		self.fields['item_name'].widget.attrs.update({'class' : 'form-control'})
		self.fields['store'].widget.attrs.update({'class' : 'form-control'})
		self.fields['proof_of_price'].widget.attrs.update({'class' : 'form-control'})
		self.fields['is_sale_price'].widget.attrs.update({'class' : 'form-check-input'})
		self.fields['image_of_item'].widget.attrs.update({'class' : 'form-control'})

class SearchForm(forms.Form):
	search = forms.CharField()

class NewStoreForm (forms.ModelForm):
	class Meta:
		model = Store
		fields= {
			"name", "store_location"
		}
	def __init__(self, *args, **kwargs):
		super(NewStoreForm, self).__init__(*args, **kwargs)
		self.fields['name'].widget.attrs.update({'class' : 'form-control'})
		self.fields['store_location'].widget.attrs.update({'class' : 'form-control'})



class NewStoreNameForm(forms.ModelForm):
	class Meta:
		model = StoreName
		fields= {
			"name"
		}

	def __init__(self, *args, **kwargs):
		super(NewStoreNameForm, self).__init__(*args, **kwargs)
		self.fields['name'].widget.attrs.update({'class' : 'form-control'})